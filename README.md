# dolphiq-craft3-forms-viewer plugin for Craft CMS 3.x

Simple plugin to expose contact form logs within Craft CMS.

![Screenshot](resources/img/plugin-logo.png)

## Requirements

This plugin requires Craft CMS 3.0.0-beta.23 or later.

## Installation

To install the plugin, follow these instructions.

1. Open your terminal and go to your Craft project:

        cd /path/to/project

2. Then tell Composer to load the plugin:

        composer require idontmessabout/dolphiq-craft3-forms-viewer

3. In the Control Panel, go to Settings → Plugins and click the “Install” button for dolphiq-craft3-forms-viewer.

## dolphiq-craft3-forms-viewer Overview

-Insert text here-

## Configuring dolphiq-craft3-forms-viewer

-Insert text here-

## Using dolphiq-craft3-forms-viewer

-Insert text here-

## dolphiq-craft3-forms-viewer Roadmap

Some things to do, and ideas for potential features:

* Release it

Brought to you by [Andy Stones](https://www.blumin.co.uk)
