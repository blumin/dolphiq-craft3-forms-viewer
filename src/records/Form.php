<?php
/**
 * dolphiq-craft3-forms-viewer plugin for Craft CMS 3.x
 *
 * Simple plugin to expose contact form logs within Craft CMS.
 *
 * @link      https://www.blumin.co.uk
 * @copyright Copyright (c) 2018 Andy Stones
 */

namespace blumin\dolphiqcraft3formsviewer\records;

use Craft;
use craft\db\ActiveRecord;

/**
 * @author    Andy Stones
 * @package   Dolphiqcraft3formsviewer
 * @since     1.0.0
 */
class Form extends ActiveRecord
{
    // Public Static Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%dq_form_log}}';
    }

    public function list()
    {
        $submissions = (new craft\db\Query())
            ->select(['id', 'form_data', 'dateCreated'])
            ->from(['{{%dq_form_log}}'])
            ->orderBy(['dateCreated' => SORT_DESC])
            ->all();

        $processed = [];

        foreach($submissions as $submission) {
            $submission['form_data'] = json_decode($submission['form_data'], true);

            // We'll parse the form data into meaningful information
            // that we can better access in the UI.
            if (isset($submission['form_data']['type'])) {
                $submission['type'] = $submission['form_data']['type'];
                unset($submission['form_data']['type']);
            } else {
                $submission['type'] = null;
            }

            if (isset($submission['form_data']['name'])) {
                $submission['name'] = $submission['form_data']['name'];
                unset($submission['form_data']['name']);
            } else if (isset($submission['form_data']['firstname'])) {
                $submission['name'] = $submission['form_data']['firstname'];
                unset($submission['form_data']['firstname']);

                if (isset($submission['form_data']['surname'])) {
                    $submission['name'] .= ' '.$submission['form_data']['surname'];
                    unset($submission['form_data']['surname']);
                }
            } else {
                $submission['name'] = null;
            }

            $submission['contact'] = [];

            if (isset($submission['form_data']['email'])) {
                $submission['contact']['email'] = $submission['form_data']['email'];
                unset($submission['form_data']['email']);
            }

            if (isset($submission['form_data']['phone'])) {
                $submission['contact']['phone'] = $submission['form_data']['phone'];
                unset($submission['form_data']['phone']);
            }

            if (isset($submission['form_data']['signup'])) {
                $submission['signup'] = $submission['form_data']['signup'];
                unset($submission['form_data']['signup']);
            } else {
                $submission['signup'] = null;
            }

            $submission['date'] = \DateTime::createFromFormat('Y-m-d H:i:s', $submission['dateCreated']);

            $processed[] = $submission;
        }

        return $processed;
    }
}
