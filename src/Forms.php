<?php

namespace blumin\dolphiqcraft3formsviewer;

use Craft;

use blumin\dolphiqcraft3formsviewer\records\Form;

use craft\base\Plugin;
use craft\web\twig\variables\CraftVariable;
use yii\base\Event;

class Forms extends Plugin
{

    // Static Properties
    // =========================================================================
    /**
     * @var Connect
     */
    public static $plugin;

    // Public Properties
    // =========================================================================
    /**
     * @var string
     */
    public $schemaVersion = '1.0.0';
    
    public function init ()
    {
        parent::init();
        self::$plugin = $this;

        Event::on(
            CraftVariable::class,
            CraftVariable::EVENT_INIT,
            function (Event $event) {
                /** @var CraftVariable $variable */
                $variable = $event->sender;
                $variable->set('forms', Form::class);
            }
        );

    }

}